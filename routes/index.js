var express = require('express'),
	router = express.Router(),
	viaContent = require('../models/viaContent'),
	trailerAddict = require('../models/trailerAddict'),
	Promise = require('bluebird');

router.get('/:region/:category/:slug', function(req, res, next) {
	var params = req.params;
	res.render('index', { title: 'Viasat', params: params });
});

router.get('/via-content/:region/:category/:slug', function(req, res, next) {
	viaContent.get(req)
		.then(function (reply) {
			res.json(reply);
		})
		.catch(function (err) {
			console.log(err);

			if (err.error) {
				//quick and naive error handling: if the via content API does not deliver what we want - display a message saying that we did not find any product
				res.status(404);
				res.json(err);
			}
		});
});

router.get('/trailer-addict/:imdb', function(req, res, next) {
	trailerAddict.get(req)
		.then(function (reply) {
			res.json(reply);
		})
		.catch(function (err) {
			console.log(err);
		});
});

module.exports = router;