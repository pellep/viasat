var rp = require('request-promise'),
	trailerAddictUrl = 'http://api.traileraddict.com/',
	Promise = require('bluebird'),
	xml2js = require('xml2js');

function parseXML(data) {
	return new Promise(function (resolve, reject) {
		var parser = new xml2js.Parser();

		parser.parseString(data, function (err, result) {
			resolve(result.trailers.trailer);
		});
	});
}


function get(request) {
	return new Promise(function (resolve) {
		return rp({
	        	url: trailerAddictUrl+'?imdb='+request.params.imdb+'&count=1&credit=no&width=000',
		        method: 'GET',
			    }).then(function (data) {
			    	return parseXML(data);
			    }).then(function (data) {
			    	resolve({iframe: data})
		    });
	});
}

exports.get = get;