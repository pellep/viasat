var rp = require('request-promise'),
	viaContentUrl = 'https://content.viaplay.se',
	Promise = require('bluebird');

function get(request) {
	return new Promise(function (resolve, reject) {
		return rp({
	        	url: viaContentUrl+'/'+request.params.region+'/'+request.params.category+'/'+request.params.slug,
		        method: 'GET',
		    }).then(function (data) {
		    	var blocks = [],
		    		reply = {};

				data = JSON.parse(data);
				blocks = data['_embedded']['viaplay:blocks'];

				blocks.forEach(function (block) {
					if (block.type === 'product') {
						reply = block['_embedded']['viaplay:product'];
					}
				});

				resolve(reply);
			}).catch(function () {
				reject({error: 'We could not find any product'});
			});
	});
}

exports.get = get;