# Viasat task #

## What tools were used? ##
On the server side I used Node together with Express 4 and the Express generator. The API requests to ViaContent and TrailerAddict was done with help of the NPM request-promise, which makes a HTTP request and delivers the response as a promise. You will find the API implementations in a folder called 'models', which is a bit misleading, since they are more of a controller content and not really model descriptions.

For CSS Preprocessing I have used Stylus. As a template engine in Express I have used EJS.

On the client side I have used AngularJS. When the view is initiated, a REST call is made to the local server with help from the ngResource library. The scope variables is getting populated with the response from that promise chain to be displayed in the view...

## What is not done? ##
On a task like this, you would like to spend days to make everything look good and to make the code as efficient and good as possible. But to be able to deliver something on just a few hours - I had to limit my work a lot.

On the server side, I would like to build some units tests and do some caching on the local REST routes.

On the client side, I would love to add some more functionality, make the site totally responsive and do some more detailed work when implementing the design. I did not use the correct font files that MTG has and since the only design to follow was a picture in a PDF, the colors and proportions is not 100% correct.

## Browser testing ##
I have not had any time for browser testing etc. But I know that the player might look a bit weird in Chrome for Linux. I believe there are some issues with using 'vw' as a measure in the CSS for that browser...

## Run the app ##
Clone the repository, go to your terminal and type **npm install**, when that is done type **npm start**. Then go to your browser and try it out: "http://localhost:3000/web-se/film/fifty-shades-of-grey-2015"