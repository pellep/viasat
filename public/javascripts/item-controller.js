

app.controller('viewController', ['$scope', '$sce', '$resource', '$location', 'ViaContent', 'TrailerAddict', function ($scope, $sce, $resource, $location, ViaContent, TrailerAddict) {
	$scope.slug = '',
	$scope.region = '';
	$scope.category = '';
	$scope.content = {};
	$scope.iframe = '';
	$scope.image = '';
	$scope.playIframe = $sce.trustAsHtml('');
	$scope.playerButtonClass = 'play-button';
	$scope.playerClass = 'player';
	$scope.availableInHd = false;
	$scope.errorMessage = '';

	var params = $location.path().split("/");

	$scope.slug = params[3];
	$scope.region = params[1];
	$scope.category = params[2];

	function setItemInfo(info) {
		$scope.image = info.content.images.landscape.url;
		$scope.content = info.content;
		$scope.genres = info['_links']['viaplay:genres'];
		
		angular.forEach(info['system']['flags'], function (flag) {
			if (flag.toLowerCase() === 'hd') {
				$scope.availableInHd = true;
			}
		});
	}

	function setIframe(data) {
		if (data.iframe) {
			$scope.iframe = data.iframe[0].embed[0];
			$scope.playButtonClass = 'play-button visible';
		}
	}

	$scope.getContent = function () {
		return ViaContent.get({
			slug: $scope.slug,
			region: $scope.region,
			category: $scope.category

		}).$promise
		.then(function (data) {
			setItemInfo(data);
			var trailerWidth = document.getElementById('trailer').clientWidth;

			return TrailerAddict.get({
					imdb: data.content.imdb.id.substr(2)
				}).$promise;

		}).then(function (data) {
			setIframe(data);
		}).catch(function (err) {
			$scope.errorMessage = err.data.error;
		});
	};

	$scope.playTrailer = function () {
		$scope.playIframe = $sce.trustAsHtml($scope.iframe);
		$scope.playerClass = 'player visible';
		$scope.playerButtonClass = 'play-button';
	}
}]);