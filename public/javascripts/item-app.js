var app = angular.module('viasat', ['ngResource', 'ngSanitize'], function ($locationProvider) {
    $locationProvider.html5Mode(true);
});