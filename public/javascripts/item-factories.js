angular.module('viasat').factory('ViaContent', ['$resource',
	function ($resource) {
		return $resource('via-content/:region/:category/:slug', 
				{
					slug: '@slug',
					region:'@region',
					category: '@category'
			});
	}
]);

angular.module('viasat').factory('TrailerAddict', ['$resource',
	function ($resource) {
		return $resource('trailer-addict/:imdb', 
				{
					imdb: '@imdb',
			});
	}
]);